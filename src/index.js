import React from 'react';
import ReactDOM from 'react-dom';
import promise from 'redux-promise';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import CategoryIndex from './components/categories_index';
import ProductsIndex from './components/products_index';
import CategoryShow from './components/categories_show';

import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Route path="/" component={ CategoryIndex } />
        <Route path="/c/:id" component={ ProductsIndex } />
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
