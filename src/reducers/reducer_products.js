import { FETCH_PRODUCTS } from '../actions';
import _ from 'lodash';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_PRODUCTS:
      return _.mapKeys(action.payload.data.items, 'id')
    default:
      return state
  }
}
