import { FETCH_CATEGORIES } from '../actions';
import _ from 'lodash';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_CATEGORIES:
      // todo: Maak een lijstje in een lijstje (childcategories). Moet misschien op een andere plek though
      return _.mapKeys(action.payload.data.children_data, 'id')
    default:
      return state
  }
}
