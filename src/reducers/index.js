import { combineReducers } from 'redux';

import CategoriesReducer from './reducer_categories';
import ProductsReducer from './reducer_products';

const rootReducer = combineReducers({
  categories: CategoriesReducer,
  products: ProductsReducer
});

export default rootReducer;
