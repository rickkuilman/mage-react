import React, {Component} from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchProducts } from '../actions';

const PRODUCTS_ID_TMP = 1;

class ProductIndex extends Component {

  // Function that is automatically executed once it's loaded in the DOM,
  // thus making it the perfect place to load the initial state
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchProducts(id);
  }

  renderProducts() {
    return _.map(this.props.products, product => {

      return (
        <div className="col-sm-6 col-md-4" key={product.sku}>
          <div className="thumbnail">
            <img src="https://supportkb.webhosting.be/admin/file/21001/5757e03fe3dc00e98815cfe5" />
            {/* <img src={`http://headlessmagento.magento2.dev/media/catalog/product/cache/f073062f50e48eb0f0998593e568d857/${product.custom_attributes[1].value}`} /> */}
          </div>
          <div className="caption">
            <h3>Product Title</h3>
            <p><a href="#" className="btn btn-primary" role="button">View product</a></p>
          </div>
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        <Link to="/c/14">
        Bestaande product index (14)
      </Link>
      <Link to="/c/15">
      Bestaande product index (15)
    </Link>
      <div className="row">
        { this.renderProducts() }
      </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { products: state.products }
}

export default connect(mapStateToProps, { fetchProducts } )(ProductIndex);
