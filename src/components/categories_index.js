import React, {Component} from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchCategories } from '../actions';

class CategoryIndex extends Component {

  // Function that is automatically executed once it's loaded in the DOM,
  // thus making it the perfect place to load the initial state
  componentDidMount() {
    this.props.fetchCategories();
  }

  renderDropdown() {
    null
  }

  renderCategories() {

    return _.map(this.props.categories, category => {
      return (
        <li key={category.id}>
          <Link to={`/c/${category.id}`} >
            {category.name}
          </Link>
        </li>
      );
    });
  }

  render() {
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand" href="#">Mage React</a>
          </div>
          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav">
              { this.renderCategories() }
            </ul>
          </div>

        </div>
      </nav>
    );
  }
}

function mapStateToProps(state) {
  return { categories: state.categories }
}


export default connect(mapStateToProps, { fetchCategories } )(CategoryIndex);
