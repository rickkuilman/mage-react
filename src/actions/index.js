import axios from  'axios';

export const FETCH_CATEGORIES = 'fetch_categories';
export const FETCH_PRODUCTS = 'fetch_products';

const BASE_URL = 'https://188.226.137.116/rest/V1';

export function fetchCategories() {

  const request = axios.get(`${BASE_URL}/categories`);

  return {
    type: FETCH_CATEGORIES,
    payload: request
  }
}

export function fetchProducts(categoryId) {

  const request = axios.get(`${BASE_URL}/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=${categoryId}&searchCriteria[filter_groups][0][filters][0][condition_type]=eq&searchCriteria[filter_groups][1][filters][0][field]=type_id&searchCriteria[filter_groups][1][filters][0][value]=configurable&searchCriteria[filter_groups][1][filters][0][condition_type]=eq`);

  return {
    type: FETCH_PRODUCTS,
    payload: request
  }
}
